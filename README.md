 # Curious Penguins

Various projects focused on merging art and science with open-source tools. Check out the website over at: https://curiouspenguins.com

## Description
With the acceleration of new technology and the rise of Web 3.0, we can’t lose sight of sustainability and protecting the environment. Curious Penguins were created specifically for this reason. Holders of Curious Penguins care about the future of the planet and are concerned with global climate change.

## Visuals
<img src="https://gitlab.com/ptoone/curious-penguins/-/raw/main/public/images/Xylologist-039.png">

## Support
Any questions related to the project can be discussed in the discord community.

## Roadmap
Welcome to the Penguin-verse: Holders can come together in the metaverse to share a mutual respect for science and technology. 

Join the Scientific Community: Each month, there will be an accelerator where holders can decide on how open source projects can be used to provide scientific solutions to real-world problems. A community wallet will be set up to fund the best projects. 

Adopt a Penguin: Each NFT will be your ticket to adopt a penguin and support the longevity of their habitat.  

Play and Earn: We will release a Curious Penguin game in the metaverse where penguins can interact and produce new tokens.
  
Curious Penguin Merch Store: Release of the Curious Penguins merch store including hats, hoodies, t-shirts. 30% of revenue will go back to the Curious Penguins community. The online store delivers globally.  

Time to Show Off: An actual print of the NFT will be shipped and delivered to your doorstep so you can show off your support for the environment. 

Let’s Get Together: Monthly organized meetings for the community to discuss sustainability in different locations around the world. Together we aim to build online and physical spaces that encourage creativity through collaboration.

## Contributing
Curious Penguins is open to contributions as we build web3 infrastruture. 

For people who want to make changes to your project, please reach out for more on how to get started. 
You will need a GitLab.com account to make any code changes and document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. 

## Projects
We are curious about ideas and focus on open source projects. Here are some Curiuos Penguins projects that you can become involved in.<br>
https://gitlab.com/curiouspenguins/crypto.txt<br>
https://wtp.curiouspenguins.com/<br>
https://otp.curiouspenguins.com/<br>
https://img.curiouspenguins.com/<br>
https://ai.curiouspenguins.com/<br>
https://ads.curiouspenguins.com/<br>
https://qr.curiouspenguins.com/<br>
https://meme.curiouspenguins.com/<br>
https://olt.curiouspenguins.com/<br>
https://poetry.curiouspenguins.com/<br>
https://stories.curiouspenguins.com/<br>


## Authors and acknowledgment
Thanks to everyone that has contributed to this project. Especially...<br>

Chaker Khazaal<br>
Omar Kandil<br>
Katharine Gordon<br>
Davey Harris<br>
Karen Arnold<br>
Atikur Rahman<br>

## License
We aim to utlize a open source first approach with a focus on Linux. 

## Project status
Active development. 
